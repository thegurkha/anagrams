//
// function definitions
//
int StartAnagramSearch(char *SourcePhrase);
int CheckForAllLetters(char *Source, char *Test, char *Remainder);
static int CompareElements(const void *First, const void *Second);
int DoCouplings(char *SourcePhrase);
int FindCompositeMatches(char *SourcePhrase);
int FindExactMatches(char *SourcePhrase);
int LetterCount(char *TestString);
int PrepareString(char *Text);
int ScanForFillers(char *SourcePhrase, char *Current, char *LastWord);
int DoListWalk(void);

// handy defines
#define		EndOfString		0x00
#define		SPACE				0x20

// database handle
sqlite3 *WordsDb = NULL;

// structure used in linked list of potential words
struct Wlink {
	char *Word;
	int Length;
	struct Wlink *Next;
};

// pointers to the start and end of the linked list
struct Wlink *Root, *End;
