#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#include <sqlite3.h>

#include "ana.h"

int main(int argc, char **argv)
{
	int nResponse;
	char strSearchPhrase[200];

	// did we get a source phrase to work with?
	if (argc != 2) {
		printf("Usage: %s SourcePhrase <enter>\n", argv[0]);
		exit(1);
	}

	// intialise the SQLite library
	sqlite3_initialize();

	// open the words database
	nResponse = sqlite3_open_v2("WordsDb.sl3", &WordsDb, SQLITE_OPEN_READWRITE, NULL);

	// if it didn't open OK, shutdown and exit
	if (nResponse != SQLITE_OK) {
		printf("%s Couldn't open database.\n", argv[0]);
		sqlite3_close(WordsDb);
		exit (-1);
	}

	// take a working copy of the text to search for anagrams for
	strcpy(strSearchPhrase, argv[1]);

	// look for anagrams
	StartAnagramSearch(strSearchPhrase);

	// shut down and exit
	sqlite3_close(WordsDb);
	sqlite3_shutdown();

	return (0);

}	// end of main

int StartAnagramSearch(char *SourcePhrase)
{
	// look for single word anagrams
	FindExactMatches(SourcePhrase);

	// show them, if any found
	DoListWalk();

	// find words which could be used in possible multiple word anagrams
	FindCompositeMatches(SourcePhrase);

	// find valid multiple word anagrams
	DoCouplings(SourcePhrase);

	// return from function when done
	return (0);

}	// end of StartAnagramSearch

int PrepareString(char *Text)
{
	int i;

	/*
	 * run through the length of the string and check that all characters
	 * are letters and not punctuation, numbers or anything else
	 *
	 */

	for (i=0; Text[i] != EndOfString; i++) {
		if (!isalpha(Text[i])) {
			printf("Error: You can only enter alphabetic characters in the SourcePhrase.\n");
			return (1);
		}
		else
				Text[i]=toupper(Text[i]);
	}

	return (0);

}	/* end of PrepareString */

static int CompareElements(const void *First, const void *Second)
{
	const char *FirstChar = (char *) First;
	const char *SecondChar = (char *) Second;

	// used by the qsort() as the comparative test between
	// two characters in the string it is sorting

	if (*FirstChar == *SecondChar)
			return (0);
	else if (*FirstChar < *SecondChar)
			return (-1);
	else
			return (1);

}	/* end of CompareElements */

int AddWordToList(char *NewWord)
{
	struct Wlink *wPtr;

	if (Root == NULL) {
		// no words in the list yet
		wPtr = (struct Wlink *) malloc(sizeof(struct Wlink));

		// can't allocate memory
		if (wPtr == NULL) {
			printf("Error: Can't allocate memory for %s in linked list.\n", NewWord);
			return (1);
		}

		// set this primary as the start of the Table
		Root=End=wPtr;
	}
	else {
		// Primaries exist, append new primary to the list

		wPtr = (struct Wlink *) malloc(sizeof(struct Wlink));

		// can't allocate memory
		if (wPtr == NULL) {
			printf("Error: Can't allocate memory for %s in linked list.\n", NewWord);
			return (1);
		}

		// point the hitherto last word to the new last word
		End->Next=wPtr;

		// this is now the end of the linked list
		End=wPtr;
	}

	// populate the word field
	wPtr->Word=strdup(NewWord);

	// can't allocate memory
	if (wPtr->Word == NULL) {
		printf("Error: Can't allocate memory for wPtr->Word for %s.\n", NewWord);
		return (1);
	}

	// store the length of the newly added word
	wPtr->Length=strlen(wPtr->Word);

	// nothing comes after this link
	wPtr->Next=NULL;

	// return from function
	return (0);

}	// end of AddWordToList

int DoListWalk(void)
{
	struct Wlink *wPtr;

	/*
	 * From the start of the linked list (root), display the word in each structure
	 * and then follow the pointer in the current structure to the next structure
	 * and display that word, and so on
	 *
	 */
	for (wPtr=Root; wPtr != NULL; wPtr=wPtr->Next)
			printf("%s\n", wPtr->Word);

	// return from the function
	return (0);

}	// end of DoListWalk

int FindExactMatches(char *SourcePhrase)
{
	char SQLString[200] = "";
	char strOriginal[200] = "";

	sqlite3_stmt *stmt = NULL;
	char *data = NULL;

	int nResponse = 0;
	int nLength = strlen(SourcePhrase);

	PrepareString(SourcePhrase);
	strcpy(strOriginal, SourcePhrase);

	// quicksort the string into alphabetical order
	qsort(SourcePhrase, nLength, sizeof(char), CompareElements);

	// create the SQL search string
	sprintf(SQLString, "SELECT * FROM words WHERE alpha=\'%s\'", SourcePhrase);

	// execute the SQL command
	nResponse = sqlite3_prepare_v2(WordsDb, SQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
		return (-1);

		// go to the first line of the result set
	nResponse = sqlite3_step(stmt);

	// keep looping as long as we are on a valid row
	while (nResponse == SQLITE_ROW) {

		// get a pointer to the data
		data = (char*)sqlite3_column_text(stmt, 1);

		// as long as it is different from the orignal word, store it as anagram
		if (strcmp(data, strOriginal) != 0)
				AddWordToList(data);

		// try to move to the next row
		nResponse = sqlite3_step(stmt);
	}

	// don't need the SQL statement anymore
	sqlite3_finalize(stmt);

	// return from function
	return (0);

}	// end of FindExactMatches

int FindCompositeMatches(char *SourcePhrase)
{
	char SQLString[200] = "";
	char strRemainder[200] = "";
	int i, nLength, nResponse = 0;

	sqlite3_stmt *stmt = NULL;
	char *data = NULL;

	nLength=strlen(SourcePhrase);

	for (i=nLength-1; i >= 1; i--) {

		sprintf(SQLString, "SELECT * FROM words WHERE length=%d", i);

		nResponse = sqlite3_prepare_v2(WordsDb, SQLString, -1, &stmt, NULL);

		if (nResponse != SQLITE_OK)
				return (-1);

		nResponse = sqlite3_step(stmt);

		while (nResponse == SQLITE_ROW) {

			data = (char*)sqlite3_column_text(stmt, 1);

			if (CheckForAllLetters(SourcePhrase, data, strRemainder) == 0) {

				if (AddWordToList(data) == 1)
						return (1);
			}

			nResponse = sqlite3_step(stmt);
		}
	}

	sqlite3_finalize(stmt);

	// return from function
	return (0);

}	// end of FindCompositeMatches

int DoCouplings(char *SourcePhrase)
{
	struct Wlink *wPtr;

	for (wPtr=Root; wPtr != NULL; wPtr=wPtr->Next)
			ScanForFillers(SourcePhrase, wPtr->Word, wPtr->Word);

	// return from function
	return (0);

}	// end of DoCouplings

int CheckForAllLetters(char *Source, char *Test, char *Remainder)
{
	int i, j, nAllPresent=0;
	char *Offset, *SourceText;

	/* make a copy of the string to test */
	SourceText=strdup(Source);

	for (i=0; Test[i] != EndOfString; i++) {

		if (Test[i] == SPACE)
				continue;

		/* perform test for presence of letter Test[i] in Source */
		Offset=strchr(SourceText, Test[i]);

		/* if not found, exit from the loop */
		if (Offset == NULL) {
			nAllPresent=1;
			break;
		}

		*Offset='.';
	}

	if (nAllPresent == 0) {
		for (i=0, j=0; SourceText[i] != EndOfString; i++) {
			if ((SourceText[i] != '.') && (SourceText[i] != '-'))
					Remainder[j++]=SourceText[i];
		}

		Remainder[j]=EndOfString;
	}

	// release the memory used by the SourceText string
	free(SourceText);

	// send back the result
	return (nAllPresent);

}	// end of CheckForAllLetters

int LetterCount(char *TestString)
{
	int i, nCount;

	for (nCount=0, i=0; TestString[i] != EndOfString; i++)
			nCount = (TestString[i] == SPACE) ? nCount : nCount+1;

	return (nCount);

}	/* end of LetterCount */

int ScanForFillers(char *SourcePhrase, char *Current, char *LastWord)
{
	struct Wlink *wPtr, *oPtr, *sPtr;
	char ThisTest[200], Remainder[200];
	int nLength, nResponse;

	nLength=strlen(LastWord);

	for (oPtr=Root, sPtr=Root; sPtr != NULL; sPtr=sPtr->Next) {

		if (sPtr->Length > nLength) {
			oPtr=oPtr->Next;
			continue;
		}

		if (strcmp(LastWord, sPtr->Word) == 0)
				break;
		else
				oPtr=oPtr->Next;
	}

	for (wPtr=oPtr; wPtr != NULL; wPtr=wPtr->Next) {

		// add this word to the test phrase
		sprintf(ThisTest, "%s %s", Current, wPtr->Word);

		// check that all the letters in the possible word are
		// in the letters in what's left of the source phrase
		nResponse=CheckForAllLetters(SourcePhrase, ThisTest, Remainder);

		// test phrase can't be made out of the letters in SourcePhrase
		if (nResponse == 1)
				continue;
		else {
			// word does fit and it uses up all of the remaining letters
			if ((int) LetterCount(ThisTest) == (int) strlen(SourcePhrase)) {
				printf("%s\n", ThisTest);
				strcpy(ThisTest, Current);
			}
			// word does fit but there are letters left
			else {
				ScanForFillers(SourcePhrase, ThisTest, wPtr->Word);
				continue;
			}
		}
	}

	return (0);

}	// end of ScanForFillers
